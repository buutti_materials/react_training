import * as React from 'react';

interface IProps {
}

class App extends React.Component<IProps> {
    public render() {
        return (
            <h1>Hello world!</h1>
        );
    }
}

export default App;