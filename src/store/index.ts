import { createStore, applyMiddleware, Store, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

export function configureStore(): Store<{}> {
    const create = createStore;

    /* composeWithDevTools is a component needed by redux dev tools, which enable viewing of the
        store in Chrome dev tools. It's not needed for production.
     */
    const store = createStore(
        combineReducers<{}>({
            root: (state = {}, action) => state
        }),
        {},
        composeWithDevTools(applyMiddleware(thunk))) as Store<{}>;

    return store;
}
